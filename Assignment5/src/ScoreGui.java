import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class ScoreGui extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	final int SIZE=6;

	JButton jbtCalculate = new JButton("Calculate");
	JButton jbtExit = new JButton("Exit");
	//score text fields
	JTextField[] jtxtScore = new JTextField[SIZE];
	JTextField[] jtxtWeight = new JTextField[SIZE]; 
	
	JLabel [] jlblModuleName= new JLabel[SIZE];

	
	JPanel scorePanel= new JPanel();
	JPanel buttonPanel= new JPanel();
	
	double[] scores = new double[SIZE];//use for scoreCalculator method
	double[] weight = new double[SIZE];// use for scoreCalculator method

	
	/**
	 * Construct the graphic user interface for score calculator
	 */
	public ScoreGui(){
		
		scorePanel.setLayout(new GridLayout(7,3));
		
		jlblModuleName[0]= new JLabel ("Test One");
		jlblModuleName[1]= new JLabel ("Test two");
		jlblModuleName[2]= new JLabel ("Test three");
		jlblModuleName[3]= new JLabel ("Test four");
		jlblModuleName[4]= new JLabel ("Grade Average");
		jlblModuleName[5]= new JLabel ("Gade Letter");
		
		
		
		
		for(int i=0; i<SIZE; i++){
			scorePanel.add(jlblModuleName[i]);
			jtxtScore[i]= new JTextField(8);
			scorePanel.add(jtxtScore[i]);
			jtxtWeight[i]= new JTextField(8);
			scorePanel.add(jtxtWeight[i]);
			
		}
		//button panel
		jbtCalculate.addActionListener(this);
		jbtExit.addActionListener(this);
		buttonPanel.add(jbtCalculate);
		buttonPanel.add(jbtExit);
		
	
		
		add(scorePanel,BorderLayout.CENTER);
		add(buttonPanel,BorderLayout.SOUTH);
		
	
		setVisible(true);
		pack();
		setLocationRelativeTo(null);
	}
	/**
	 * Calculate grade average scores
	 * @param score an array of scores
	 * @param weight an array of weights
	 * @return grade average score
	 */

	public double calculateScore( double []scores, double [] weight){
		double grade= 0.0;
		//logic to calculate
		//need to modify the code below to support the correct logic
		
		grade=  (scores[0] * weight[0]+ scores[1]* weight[1]+ scores[2]*weight[2]+scores[3]*weight[3]);
		
		return grade;
	}
	/**
	 * calculate grade letter
	 * @param score grade average score
	 * @return letter grade A,B,C,D,F
	 */
	public char calculateLetterGrade(double score){
		//logic to find letter grade
		// use if.. else or switch case
		
		if(score >=90 && score <= 100){
			return 'A';
		} else if(score>=80 && score <= 89){
			return 'B';
		}else if(score>=70 && score <= 79){
			return 'C';
		}else if (score >=65 && score <= 69 ){
			return 'D';
		}else if (score>=64 && score <= 0){
			return 'F';
		}
		else
		{
			return 'N';
		}
		}

	
@Override
public void actionPerformed(ActionEvent e) {
	for(int i= 0; i< 4; i++ ){
	
	scores[i]=Double.parseDouble(jtxtScore[i].getText());
	weight[i]=Double.parseDouble(jtxtWeight[i].getText());
}
	
    jtxtScore[4].setText(calculateScore(scores,weight)+ "");
    jtxtScore[5].setText(calculateLetterGrade(calculateScore(scores,weight)) + "");
}
}